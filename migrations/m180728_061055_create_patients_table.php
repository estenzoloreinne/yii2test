<?php

use yii\db\Migration;

/**
 * Handles the creation of table `patients`.
 */
class m180728_061055_create_patients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('patients', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('patients');
    }
}
