<?php

use yii\db\Migration;

/**
 * Class m180728_063333_add_columns_to_patients_table
 */
class m180728_063333_add_columns_to_patients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('patients', 'age', $this->integer());
        $this->addColumn('patients', 'name', $this->string());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180728_063333_add_columns_to_patients_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180728_063333_add_columns_to_patients_table cannot be reverted.\n";

        return false;
    }
    */
}
