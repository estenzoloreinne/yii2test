<?php
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;

?>

<div class="patients-default-index">
    <center><h2>Patients</h2></center>
    <br>
    <table class="table table-bordered">
    <thead>
      <tr>
        <th>Name</th>
        <th>Age</th>
      </tr>
    </thead>
    <tbody>
        <?php foreach($patients as $p) 
      echo '<tr>';
        echo '<td>'.$p->name. '</td>';
        echo '<td>'.$p->age.'</td>';
      echo '</tr>';
       ?>
    </tbody>
  </table>
</div>