<?php

namespace app\patients\controllers;

use yii\web\Controller;
use app\models\Patients;

/**
 * Default controller for the `patients` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
    	$patients = Patients::find()->all();
        return $this->render('index', [
        	'patients' => $patients]);
    }

}
