<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "patients".
 *
 * @property int $id
 * @property int $age
 * @property string $name
 */
class Patients extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public static function getDb()
    {
        return \Yii::$app->db;  
    }

    public static function tableName()
    {
        return 'patients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['age'], 'default', 'value' => null],
            [['age'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'age' => 'Age',
            'name' => 'Name',
        ];
    }

      public function getId()
    {
        return $this->id;
    }
}
